const mongoose = require("mongoose");

const courierSchema = new mongoose.Schema({
	courierName: {
		type: String,
		required: [true, "Name is required"]
	},
	courierEmail: {
		type: String,
		required: [true, "Email is required"]
	},
	courierPassword: {
		type: String,
		required: [true, "Password is required"]
	},
	courierMobile: {
		type: String,
		required: [true, "Monile number is required"]
	},
    courierAddress: {
        type: String,
		required: [true, "Address is required"]
    },
	courierLicenseNo: {
        type: String,
        required: [true, "License is required"]
    }
})

module.exports = mongoose.model("Courier", courierSchema);