const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
    productName: {
		type: String,
		required: [true, "Product name is required"]
	},
	productDescription: {
		type: String,
		required: [true, "Product description is required"]
	},
    productPrice: {
        type: Number,
        required: [true, "Product price is required"]
    },
    productStock: {
        type: Number,
        required: [true, "Product stock is required"]
    },
    productCategory: {
        type: String,
        required: [true, "Product category is required"]
    },
    productStatus: {
        type: String,
        default: "Active"
    },
    img: { 
        data: Buffer, contentType: String 
    },
    productReviews: [
        {
            reviewScore: {
                type: Number
            },
            reviewComment: {
                type: String
            },
            reviewerId: {
                type: String
            },
            reviewerFname: {
                type: String
            },
            reviewerLname: {
                type: String
            }
        }
    ]
});

module.exports = mongoose.model("Product", productSchema);