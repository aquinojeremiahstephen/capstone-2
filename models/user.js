const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	userFname: {
		type: String,
		required: [true, "First name is required"]
	},
	userLname: {
		type: String,
		required: [true, "Last name is required"]
	},
	userEmail: {
		type: String,
		required: [true, "Email is required"]
	},
	userPassword: {
		type: String,
		required: [true, "Password is required"]
	},
	userIsAdmin: {
		type: Boolean,
		default: false
	},
	userMobile: {
		type: String,
		required: [true, "Monile number is required"]
	},
    userAddress: {
        type: String
    },
	userWishlist: [
		{
			productId: {
				type: String
			}
		}
	],
	userCart: [
		{
			productId: {
				type: String,
			},
            productName: {
                type: String
            },
            productPrice: {
                type: Number
            },
			productQty: {
				type: Number,
			},
			productSubtotal: {
				type: Number
			}
		}
	]
})

module.exports = mongoose.model("User", userSchema);