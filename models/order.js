const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
    orderBuyerId: {
        type: String,
        required: [true, "Indicate buyer Id"]
    },
    orderBuyerFname: {
        type: String
       
    },
    orderBuyerLname: {
        type: String
    },
    orderSummary: [
        {
            productId: {
				type: String
			},
            productName: {
                type: String
            },
            productCategory: {
                type: String
            },
            productPrice: {
                type: Number
            },
			productQty: {
				type: Number
			},
			productSubtotal: {
				type: Number
			}
        }
    ],
    orderTotal: {
        type: Number
    },
    orderDate: {
        type: Date,
        default: new Date()
    },
    orderStatus: {
        type: String,
        default: "To confirm"
    }
});

module.exports = mongoose.model("Order", orderSchema);

