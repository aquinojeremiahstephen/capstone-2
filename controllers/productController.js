const User = require("./../models/user.js");
const Product = require("./../models/product.js");
const Order = require("./../models/order.js");
const Courier = require("./../models/courier.js");
const auth = require("./../auth.js")
const bcrypt = require("bcrypt");

//Controller for getting all active products
module.exports.getAllActiveProduct = () => {
	return Product.find({productStatus: "Active"}).then((product, error) =>{
		return product;
	});
}

//Controller for getting specific products
module.exports.getProduct = (params) => {
	return Product.findById(params.productId).then(result => {
		return result;
	});
}

//Controller for getting a product review
module.exports.getProductReview = (params) => {
	return Product.findById(params.productId).then(result => {
		return result.productReviews;
	});
}

//Controller for adding a product review
module.exports.addProductReview = (params,body,userId) => {	
	let currentReview = {
		reviewScore: body.reviewScore,
		reviewComment: body.reviewComment,
		reviewerFname: body.reviewerFname,
		reviewerLname: body.reviewerLname,
		reviewerId: body.reviewerId,
	}

	console.log(currentReview);
    return Product.findOneAndUpdate({"_id": params.productId},{$push: {productReviews: currentReview}}).then((product, error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}
	});
}

//Controller for adding a product to wishlist
module.exports.addToWishList = (params,userId) => {	
	let currentProduct = {
		productId: params.productId,
	}
    return User.findOneAndUpdate({"_id": userId},{$push: {userWishlist: currentProduct}}).then((product, error) => {
		if(error){
			return false;
		}
		else{
			return "Product addded to wishlist!";
		}
	});
}

