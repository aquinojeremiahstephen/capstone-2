const User = require("./../models/user.js");
const Product = require("./../models/product.js");
const Order = require("./../models/order.js");
const Courier = require("./../models/courier.js");
const auth = require("./../auth.js")
const bcrypt = require("bcrypt");

const fs = require('fs');

//Controller for adding product
module.exports.adminAddProduct = (body,file) => {
	let newProduct = new Product({
		productName: body.productName,
		productDescription: body.productDescription,
		productCategory: body.productCategory,
        productPrice: body.productPrice,
        productStock: body.productStock,
		img: { data: fs.readFileSync(`./admin_upload/${file.filename}`), contentType: file.mimetype}
	});

	return newProduct.save().then((product, error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}
	});
}

//Controller for viewing all products
module.exports.adminViewAllProducts = (userId) => {
	return Product.find().then((product, error) =>{
		return product;
	});
}

//Controller for updating product
module.exports.adminUpdateProduct = (params, body) => {
	return Product.findByIdAndUpdate(params.productId, body).then((product, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}

//Controller for archiving product
module.exports.adminArchiveProduct = (params,body) => {
	let updatedProduct = undefined;

	if(body.productStatus === "Active"){
		updatedProduct = {
			productStatus: "Inactive"
		}
	}else{
		updatedProduct = {
			productStatus: "Active"
		}
	}
		
	return Product.findByIdAndUpdate(params.productId, updatedProduct).then((product, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})	
}

//Controller for viewing all orders
module.exports.adminViewToConfirm = () => {
	return Order.find({orderStatus: "To confirm"}).then((order, error) =>{
		return order;
	});
}

//Controller for viewing all orders
module.exports.adminViewToShip = () => {
	return Order.find({orderStatus: "To ship"}).then((order, error) =>{
		return order;
	});
}

//Controller for viewing all orders
module.exports.adminViewShipped = () => {
	return Order.find({orderStatus: "Shipped"}).then((order, error) =>{
		return order;
	});
}

//Controller for viewing all orders
module.exports.adminViewDelivered = () => {
	return Order.find({orderStatus: "Delivered"}).then((order, error) =>{
		return order;
	});
}

//Controller for viewing all orders
module.exports.adminViewCancelled = () => {
	return Order.find({orderStatus: "Cancelled"}).then((order, error) =>{
		return order;
	});
}

//Controller for updating order status
module.exports.adminOrderToConfirm = (params) => {
	let updatedOrder = {
		orderStatus: "To confirm"
	}

	return Order.findByIdAndUpdate(params.orderId, updatedOrder).then((product, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})	
}

//Controller for updating order status
module.exports.adminOrderToShip = (params) => {
	let updatedOrder = {
		orderStatus: "To ship"
	}

	return Order.findByIdAndUpdate(params.orderId, updatedOrder).then((product, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})	
}

//Controller for updating order status
module.exports.adminOrderShipped = (params) => {
	let updatedOrder = {
		orderStatus: "Shipped"
	}

	return Order.findByIdAndUpdate(params.orderId, updatedOrder).then((product, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})	
}

//Controller for updating order status
module.exports.adminOrderDelivered = (params) => {
	let updatedOrder = {
		orderStatus: "Delivered"
	}

	return Order.findByIdAndUpdate(params.orderId, updatedOrder).then((product, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})	
}

//Controller for updating order status
module.exports.adminOrderCancelled = (params) => {
	let updatedOrder = {
		orderStatus: "Cancelled"
	}

	return Order.findByIdAndUpdate(params.orderId, updatedOrder).then((product, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})	
}

//Controller for making user to admin
module.exports.adminMakeUserAdmin = (params) => {
	let updatedUser = {
		userIsAdmin: true
	}

	return User.findByIdAndUpdate(params.userId, updatedUser).then((user, error) => {
		if(error){
			return false;
		}else{
			return "User promoted to admin";
		}
	})	
}

//COntroller for fetching user 

module.exports.profileUserFetch = (params) => {	
    return User.findById(params.userId).then(result => {
		return result;
	});
}