const User = require("./../models/user.js");
const Product = require("./../models/product.js");
const Order = require("./../models/order.js");
const Courier = require("./../models/courier.js");
const auth = require("./../auth.js")
const bcrypt = require("bcrypt");

//Controller for checkout cart
module.exports.checkOutCart = (userId,body) => {	
    let checkOutOrder = new Order({
        orderBuyerId: userId,
        orderSummary: body.orderSummary,
		orderTotal: body.orderTotal,
		orderBuyerFname: body.orderBuyerFname,
		orderBuyerLname: body.orderBuyerLname
    });

    return checkOutOrder.save().then((order, error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}
	});
}




