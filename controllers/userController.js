const User = require("./../models/user.js");
const Product = require("./../models/product.js");
const Order = require("./../models/order.js");
const Courier = require("./../models/courier.js");
const auth = require("./../auth.js")
const bcrypt = require("bcrypt");

//COntroller for check email
module.exports.checkEmail = (body) => {
    return User.find({userEmail: body.userEmail}).then(result => {
        if(result.length > 0){
            return true;
        }else{
            return false;
        }
    });
}

//Controller for user registration
module.exports.registerUser = async (body) => {
    let newUser = new User({
        userFname: body.userFname,
        userLname: body.userLname,
        userEmail: body.userEmail,
        userPassword: bcrypt.hashSync(body.userPassword, 10),
        userMobile: body.userMobile,
        userAddress: body.userAddress
    });

    return newUser.save().then((user, error) => {
        if(error){
            return false;
        }else{
            return true;
        }
    });
  
}

//Controller for userlogin
module.exports.loginUser = (body) => {
    return User.findOne({userEmail: body.userEmail}).then(result => {
        if(result === null){
            return false;
        }else{
            const isPasswordCorrect = bcrypt.compareSync(body.userPassword, result.userPassword);

            if(isPasswordCorrect){
                return {access: auth.createAccessToken(result.toObject())}
            }else{
                return false;
            }
        }
    });
}

//Controller for viewing current user profile
module.exports.profileUser = (userId) => {	
    return User.findById(userId).then(result => {
		return result;
	});
}

//Controller for viewing my orders
module.exports.userViewMyOrders = (userId) => {
	return Order.find({orderBuyerId: userId}).then((order, error) =>{
		return order;
	});
}

//Controller for viewing "To confirm" orders
module.exports.userOrdersToConfirm = (userId) => {
	return Order.find({orderStatus: "To confirm",orderBuyerId: userId}).then((order, error) =>{
		return order;
	});
}

//Controller for viewing "To ship" orders
module.exports.userOrdersToShip = (userId) => {
	return Order.find({orderStatus: "To ship",orderBuyerId: userId}).then((order, error) =>{
		return order;
	});
}

//Controller for viewing "Shipped" orders
module.exports.userOrdersShipped = (userId) => {
	return Order.find({orderStatus: "Shipped",orderBuyerId: userId}).then((order, error) =>{
		return order;
	});
}

//Controller for viewing "Delivered" orders
module.exports.userOrdersDelivered = (userId) => {
	return Order.find({orderStatus: "Delivered",orderBuyerId: userId}).then((order, error) =>{
		return order;
	});
}

//Controller for viewing "Cancelled" orders
module.exports.userOrdersCancelled = (userId) => {
	return Order.find({orderStatus: "Cancelled",orderBuyerId: userId}).then((order, error) =>{
		return order;
	});
}

//Controller for cancelling user order
module.exports.userCancelOrder = (params,userId) => {
	return Order.findByIdAndUpdate({_id: params.orderId, orderBuyerId: userId},{"orderStatus": "Cancelled"}).then((order, error) => {
		if(error){
			return false;
		}else{
			return "Order: ("+params.orderId+") cancelled";
		}
	})
}

//Controller for viewing cart
module.exports.userViewCart = (userId) => {	
    return User.findById(userId).then(result => {
		return result.userCart;
	});
}

//Controller for viewing wishlist
module.exports.userViewWishlist = (userId) => {	
    return User.findById(userId).then(result => {
		return result.userWishlist;
	});
}

