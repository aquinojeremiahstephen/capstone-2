// Set up the dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const multer  = require('multer');

//import routes
const userRoutes = require("./routes/userRoutes.js");
const productRoutes = require("./routes/productRoutes.js");
const orderRoutes = require("./routes/orderRoutes.js");
const adminRoutes = require("./routes/adminRoutes.js");

//Server setup
const app = express();
const port = 8080;

const corsOptions = {
	origin: ['http://localhost:3000', 'https://focused-noether-17ca3f.netlify.app'],
	optionsSuccessStatus: 200
}

//Tells server that cors is being used by your server
app.use(cors(corsOptions));

//Tells server to use json data
app.use(express.json());

app.use(express.urlencoded({extended: true}));

//Define the routes being used
app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);
app.use("/admin", adminRoutes);

//Connection to MongoDB Atlas
mongoose.connect("mongodb+srv://admin:admin123@cluster0.f9oka.mongodb.net/ecommerce?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

mongoose.connection.once("open",() => console.log("Connected to MongoDB Atlas."));

//Confirms if server is now connected to the designated port
app.listen(process.env.PORT || port, () => {
	console.log(`API is now online on port ${process.env.PORT || port}`)
});
