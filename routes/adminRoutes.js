const express = require('express');
const router = express.Router();
const auth = require("./../auth.js");
const adminController = require('./../controllers/adminController.js')


const multer  = require('multer')

const storage = multer.diskStorage({
	destination: (req, file, callback) => {
		callback(null, "admin_upload/");
	},
	filename: (req, file, callback) => {
		callback(null, `${Date.now()}-${file.originalname}`);
	}
})
 
const upload = multer({storage:storage});

//Route to create new product
router.post("/catalogue/add", upload.single('productImage'), auth.verify,  (req, res) => {
	let token = auth.decode(req.headers.authorization)
	if(token.userIsAdmin === true){
		adminController.adminAddProduct(req.body,req.file).then(resultFromAdminAddProduct => res.send(resultFromAdminAddProduct));
	}else{
		res.send({auth: "Not an admin"});
	}
});

//Route to view all products
router.get("/catalogue/all", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)
    if(token.userIsAdmin === true){
		adminController.adminViewAllProducts().then(resultFromAdminViewAllProducts => res.send(resultFromAdminViewAllProducts));
	}else{
		res.send({auth: "Not an admin"});
	}
});

//Route to update specific product
router.put("/catalogue/update/:productId", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)
	if(token.userIsAdmin === true){
		adminController.adminUpdateProduct(req.params, req.body).then(resultFromAdminUpdateProduct => res.send(resultFromAdminUpdateProduct));
	}else{
		res.send({auth: "Not an admin"});
	}
});

//Route to archive product
router.put("/catalogue/archive/:productId", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)
	if(token.userIsAdmin === true){
		adminController.adminArchiveProduct(req.params, req.body).then(resultFromArchiveProduct => res.send(resultFromArchiveProduct));
	}else{
		res.send({auth: "Not an admin"});
	}
});

//Route to view all orders
router.get("/orders/all", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)
    if(token.userIsAdmin === true){
		adminController.adminViewAllOrders().then(resultFromViewAllOrders => res.send(resultFromViewAllOrders));
	}else{
		res.send({auth: "Not an admin"});
	}
});

//Route to view all orders
router.get("/orders/toConfirm", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)
    if(token.userIsAdmin === true){
		adminController.adminViewToConfirm().then(resultFromViewAllOrders => res.send(resultFromViewAllOrders));
	}else{
		res.send({auth: "Not an admin"});
	}
});

//Route to view all orders
router.get("/orders/toShip", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)
    if(token.userIsAdmin === true){
		adminController.adminViewToShip().then(resultFromViewAllOrders => res.send(resultFromViewAllOrders));
	}else{
		res.send({auth: "Not an admin"});
	}
});

//Route to view all orders
router.get("/orders/shipped", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)
    if(token.userIsAdmin === true){
		adminController.adminViewShipped().then(resultFromViewAllOrders => res.send(resultFromViewAllOrders));
	}else{
		res.send({auth: "Not an admin"});
	}
});

//Route to view all orders
router.get("/orders/delivered", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)
    if(token.userIsAdmin === true){
		adminController.adminViewDelivered().then(resultFromViewAllOrders => res.send(resultFromViewAllOrders));
	}else{
		res.send({auth: "Not an admin"});
	}
});

//Route to view all orders
router.get("/orders/cancelled", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)
    if(token.userIsAdmin === true){
		adminController.adminViewCancelled().then(resultFromViewAllOrders => res.send(resultFromViewAllOrders));
	}else{
		res.send({auth: "Not an admin"});
	}
});

//route to update order status
router.put("/orders/update/toConfirm/:orderId", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)
	if(token.userIsAdmin === true){
		adminController.adminOrderToConfirm(req.params).then(resultFromAdminUpdateOrderStatus => res.send(resultFromAdminUpdateOrderStatus));
	}else{
		res.send({auth: "Not an admin"});
	}
});

//route to update order status
router.put("/orders/update/toShip/:orderId", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)
	if(token.userIsAdmin === true){
		adminController.adminOrderToShip(req.params).then(resultFromAdminUpdateOrderStatus => res.send(resultFromAdminUpdateOrderStatus));
	}else{
		res.send({auth: "Not an admin"});
	}
});

//route to update order status
router.put("/orders/update/shipped/:orderId", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)
	if(token.userIsAdmin === true){
		adminController.adminOrderShipped(req.params).then(resultFromAdminUpdateOrderStatus => res.send(resultFromAdminUpdateOrderStatus));
	}else{
		res.send({auth: "Not an admin"});
	}
});

//route to update order status
router.put("/orders/update/delivered/:orderId", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)
	if(token.userIsAdmin === true){
		adminController.adminOrderDelivered(req.params).then(resultFromAdminUpdateOrderStatus => res.send(resultFromAdminUpdateOrderStatus));
	}else{
		res.send({auth: "Not an admin"});
	}
});

//route to update order status
router.put("/orders/update/cancelled/:orderId", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)
	if(token.userIsAdmin === true){
		adminController.adminOrderCancelled(req.params).then(resultFromAdminUpdateOrderStatus => res.send(resultFromAdminUpdateOrderStatus));
	}else{
		res.send({auth: "Not an admin"});
	}
});

//route to make user admin
router.put("/user/promote/:userId", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)
	if(token.userIsAdmin === true){
		adminController.adminMakeUserAdmin(req.params).then(resultFromAdminMakeUserAdmin => res.send(resultFromAdminMakeUserAdmin));
	}else{
		res.send({auth: "Not an admin"});
	}
});

//Route for viewing profile
router.get("/fetch/profile/:userId", auth.verify, (req, res) => {
    adminController.profileUserFetch(req.params).then(resultFromProfileUser => res.send(resultFromProfileUser));
});


module.exports = router;