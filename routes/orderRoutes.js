const express = require('express');
const router = express.Router();
const auth = require("./../auth.js");
const orderController = require('./../controllers/orderController.js')

//Route to checkout cart
router.post("/cart/checkOut", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)
    orderController.checkOutCart(token.id,req.body).then(resultFromUserCheckOutCart => res.send(resultFromUserCheckOutCart));
});





module.exports = router;