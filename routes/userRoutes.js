const express = require('express');
const router = express.Router();
const auth = require("./../auth.js");
const userController = require('./../controllers/userController.js')

//Route to check for duplicate emails
router.post("/checkEmail", (req, res) => {
    userController.checkEmail(req.body).then(resultFromEmailExists => res.send(resultFromEmailExists));
});

//Route for user registration
router.post("/register", (req, res) => {
    userController.registerUser(req.body).then(resultFromRegister => res.send(resultFromRegister));
});

//Route for user login
router.post("/login", (req, res) => {
    userController.loginUser(req.body).then(resultFromLogin => res.send(resultFromLogin));
});

//Route for viewing profile
router.get("/profile", auth.verify, (req, res) => {
    let token = auth.decode(req.headers.authorization)
    userController.profileUser(token.id).then(resultFromProfileUser => res.send(resultFromProfileUser));
});

//Route to view my orders
router.get("/orders/all", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)
    userController.userViewMyOrders(token.id).then(resultFromUserViewMyOrders => res.send(resultFromUserViewMyOrders));
});

//Route for viewing to confirm orders
router.get("/orders/toConfirm", auth.verify, (req, res) => {
    let token = auth.decode(req.headers.authorization)
    userController.userOrdersToConfirm(token.id).then(resultFromUserOrdersToConfirm => res.send(resultFromUserOrdersToConfirm));
});

//Route for viewing to ship orders
router.get("/orders/toShip", auth.verify, (req, res) => {
    let token = auth.decode(req.headers.authorization)
    userController.userOrdersToShip(token.id).then(resultFromUserOrdersToShip => res.send(resultFromUserOrdersToShip));
});

//Route for viewing to shipped orders
router.get("/orders/shipped", auth.verify, (req, res) => {
    let token = auth.decode(req.headers.authorization)
    userController.userOrdersShipped(token.id).then(resultFromUserOrdersShipped => res.send(resultFromUserOrdersShipped));
});

//Route for viewing to delivered orders
router.get("/orders/delivered", auth.verify, (req, res) => {
    let token = auth.decode(req.headers.authorization)
    userController.userOrdersDelivered(token.id).then(resultFromUserOrdersDelivered => res.send(resultFromUserOrdersDelivered));
});

//Route for viewing to cancelled orders
router.get("/orders/cancelled", auth.verify, (req, res) => {
    let token = auth.decode(req.headers.authorization)
    userController.userOrdersCancelled(token.id).then(resultFromUserOrdersCancelled => res.send(resultFromUserOrdersCancelled));
});

//Route for cancelling an order
router.put("/orders/cancelOrder/:orderId", auth.verify, (req, res) => {
    let token = auth.decode(req.headers.authorization)
    userController.userCancelOrder(req.params,token.id).then(resultFromUserCancelOrder => res.send(resultFromUserCancelOrder));
});

//Route to view cart
router.get("/cart/view", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)
    userController.userViewCart(token.id).then(resultFromUserFromViewCart => res.send(resultFromUserFromViewCart));
});

//Route to view wishlist
router.get("/wishlist", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)
    userController.userViewWishlist(token.id).then(resultFromUserViewWishlist => res.send(resultFromUserViewWishlist));
});

module.exports = router;