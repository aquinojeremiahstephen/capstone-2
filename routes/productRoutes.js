const express = require('express');
const router = express.Router();
const auth = require("./../auth.js");
const productController = require('./../controllers/productController.js')

//Route to get all active product
router.get("/catalogue/all", (req, res) => {
	productController.getAllActiveProduct().then(resultGetAllActiveProduct => res.send(resultGetAllActiveProduct));
});

//Route to get a specific product
router.get("/catalogue/profile/:productId", (req, res) => {
	productController.getProduct(req.params).then(resultFromGetProduct => res.send(resultFromGetProduct));
});

//Route to get a specific product review
router.get("/catalogue/reviews/:productId", (req, res) => {
	productController.getProductReview(req.params).then(resultFromGetProductReview => res.send(resultFromGetProductReview));
});

//Route to review specific product
router.put("/catalogue/profile/:productId", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)
	productController.addProductReview(req.params,req.body,token.id).then(resultFromAddProductReview => res.send(resultFromAddProductReview));
});

//Route add product to wishlist
router.put("/wishlist/:productId", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)
    productController.addToWishList(req.params,token.id).then(resultFromAddToWishList => res.send(resultFromAddToWishList));
});

module.exports = router;